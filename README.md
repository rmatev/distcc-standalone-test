## Test the setup
```shell
. ./setup.sh
distcc $CXX -c test.cpp
distcc $CXX -o test.exe test.o  # linking will fallback
# ...
```

```shell
# SSH port forwarding (outside CERN network)
# ssh -N -L 36321:rmatev02:3632 lxplus.cern.ch &
# export DISTCC_HOSTS="127.0.0.1:36321/6"
# perf stat -r 10 distcc $CXX -c test.cpp
```

## Benchmarks
```shell
bash benchmarks.sh
```

## Benchmark LHCb soft
### GAUDI (v29-patches, 7a580596)
```sh
source setup.sh
cd Gaudi

# Cold start without distcc and wo ccache
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="" make configure
/usr/bin/time make all
# 1977.58user 190.51system 9:51.95elapsed 366%CPU (0avgtext+0avgdata 1375052maxresident)k
# 31579360inputs+383264outputs (96942major+36376704minor)pagefaults 0swaps

# Cold start without distcc and wo ccache
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON" make configure
NINJAFLAGS="-j100" /usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../rmatev04-distcc-both.ninja_log
# 248.85user 106.25system 2:53.64elapsed 204%CPU (0avgtext+0avgdata 647064maxresident)k
# 16245984inputs+2987528outputs (38382major+7386826minor)pagefaults 0swaps



# Cold start with distcc
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON -DCMAKE_USE_CCACHE=ON" NINJAFLAGS="-j16" make configure
/usr/bin/time make all
# 475.13user 199.34system 4:23.59elapsed 255%CPU (0avgtext+0avgdata 649476maxresident)k
# 39304888inputs+5870232outputs (87549major+13667084minor)pagefaults 0swaps

# with hot ccache (and distcc, which is never called)
make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON -DCMAKE_USE_CCACHE=ON" make configure
/usr/bin/time make all
# 78.80user 27.74system 0:37.87elapsed 281%CPU (0avgtext+0avgdata 647884maxresident)k
# 9622208inputs+425968outputs (13877major+3244163minor)pagefaults 0swaps

```

#### On quanta02 directly
```sh
# Cold start without distcc and wo ccache
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="" make configure
/usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-vanilla.ninja_log
# 2412.21user 429.38system 2:22.43elapsed 1995%CPU (0avgtext+0avgdata 1381852maxresident)k
# 21417128inputs+383480outputs (294088major+57542639minor)pagefaults 0swaps

# Cold start without distcc and with ccache
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_CCACHE=ON" make configure
/usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-cold-ccache.ninja_log
# 2548.68user 783.65system 3:15.49elapsed 1704%CPU (0avgtext+0avgdata 1382428maxresident)k
# 46034728inputs+684144outputs (351647major+65359009minor)pagefaults 0swaps

# Hot start without distcc and with ccache
make purge && CMAKEFLAGS="-DCMAKE_USE_CCACHE=ON" make configure
/usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-hot-ccache.ninja_log
# 72.52user 89.58system 0:21.75elapsed 745%CPU (0avgtext+0avgdata 653148maxresident)k
# 8543104inputs+426088outputs (22026major+2971601minor)pagefaults 0swaps

# Cold start with distcc and wo ccache
export DISTCC_HOSTS="hltperf-quanta02-e52630v4:12345/40"
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON" make configure
/usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-local-distcc.ninja_log
# 282.64user 159.58system 2:32.63elapsed 289%CPU (0avgtext+0avgdata 653716maxresident)k
# 14439472inputs+406752outputs (52651major+6943557minor)pagefaults 0swaps

export DISTCC_HOSTS="hltperf-quanta02-e52630v4:12345/40 hltperf-quanta01-e52630v4:12345/40"
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON" make configure
NINJAFLAGS="-j100" /usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-distcc-both.ninja_log
# 246.98user 137.67system 2:05.19elapsed 307%CPU (0avgtext+0avgdata 653412maxresident)k
# 15124888inputs+406664outputs (49697major+6761239minor)pagefaults 0swaps
# 255.15user 144.33system 2:15.13elapsed 295%CPU (0avgtext+0avgdata 653400maxresident)k
# 15183584inputs+406272outputs (53059major+6851228minor)pagefaults 0swaps

export DISTCC_HOSTS="hltperf-quanta01-e52630v4:12345/40"
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON" NINJAFLAGS="-j50" make configure
/usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../quanta02-distcc-quanta01.ninja_log
# 218.36user 122.95system 2:12.18elapsed 258%CPU (0avgtext+0avgdata 651932maxresident)k
# 20799072inputs+406536outputs (74302major+6607858minor)pagefaults 0swaps
```
#### On action
```sh
export DISTCC_HOSTS="hltperf-quanta02-e52630v4:12345/40 hltperf-quanta01-e52630v4:12345/40"
rm -rf $CCACHE_DIR && make purge && CMAKEFLAGS="-DCMAKE_USE_DISTCC=ON" make configure
NINJAFLAGS="-j100" /usr/bin/time make all
cp build.x86_64-centos7-gcc62-opt/.ninja_log ../action-distcc-both.ninja_log
# 270.87user 128.28system 2:01.05elapsed 329%CPU (0avgtext+0avgdata 651604maxresident)k
# 20955416inputs+406456outputs (76131major+6605743minor)pagefaults 0swaps
```

### Full stack
3487.00user 1180.33system 35:09.10elapsed 221%CPU (0avgtext+0avgdata 3358648maxresident)k
273521752inputs+9845000outputs (699696major+111030470minor)pagefaults 0swaps
