TEMPLATE = """
int main() {{
    double {0};
    double x = {1};
    return 0;
}}
"""

with open('test.cpp', 'w') as f:
    decl = ','.join('a{}'.format(i) for i in range(100))
    prod = '*'.join('a{}'.format(i) for i in range(100))
    f.write(TEMPLATE.format(decl, '+\n'.join([prod] * 500)))
