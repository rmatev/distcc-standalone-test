#!/bin/bash
N=$1; shift
jobs=$1; shift

make -f parallel.mk -j $jobs N=$N CMD="$*"

# id="parallel"

# # Use tmpfs storage for semaphores (AFS home totally fails)
# if ! ln -f -s $XDG_RUNTIME_DIR/parallel/{tmp,semaphores} ~/.parallel/ ; then
#     rm -rf ~/.parallel/tmp ~/.parallel/semaphores
#     mkdir -p $XDG_RUNTIME_DIR/parallel/{tmp,semaphores}
#     ln -f -s $XDG_RUNTIME_DIR/parallel/{tmp,semaphores} ~/.parallel/
# fi

# for i in $(seq 1 $N); do
#     sem --will-cite --fg --id $id -j $jobs "$@" &
# done
# wait
# sem --will-cite --id $id --wait