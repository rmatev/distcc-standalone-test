CXX = lcg-g++-8.2.0
SRC_TEMPLATE = fast.cpp
SRC = $(shell echo src/src{001..120}.cpp)
LOCAL_OBJ = $(SRC:.cpp=.local.o)
DISTCC_OBJ = $(SRC:.cpp=.distcc.o)

.PHONY: clean prepare local distcc

prepare: clean $(SRC)
local: $(LOCAL_OBJ)
distcc: $(DISTCC_OBJ)


$(SRC) : $(SRC_TEMPLATE)
	@mkdir -p $(basename $@)
	@cp $< $@

src/%.local.o : src/%.cpp
	$(CXX) -c $< -o $@

src/%.distcc.o : src/%.cpp
	distcc $(CXX) -c $< -o $@

clean:
	@rm -f $(LOCAL_OBJ) $(DISTCC_OBJ) $(SRC)

