export DISTCC_VERBOSE=0
export DISTCC_PRINCIPAL=distccd
# export DISTCC_HOSTS_STD="lbquantaperf01.cern.ch/40,auth"
# export DISTCC_HOSTS_PUMP="lbquantaperf01.cern.ch/40,auth,cpp,lzo"
export DISTCC_HOSTS_STD="lbquantaperf02.cern.ch/40"
export DISTCC_HOSTS_PUMP="lbquantaperf02.cern.ch/40,cpp,lzo"
export DISTCC_HOSTS=$DISTCC_HOSTS_STD
export CXX="lcg-g++-8.2.0"
rm -rf ~/.distcc  # remove state (e.g. backoff)