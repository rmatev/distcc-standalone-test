#!/bin/bash

. ./setup.sh

compile_cmd="$CXX -c fast.cpp -o /dev/null"
remote_host=lbquantaperf02
nproc_local=$(nproc)
nproc_remote=40
scp fast.cpp parallel.mk parallel.sh timeit ptime.py $remote_host:/tmp/ >/dev/null

echo "### Serial"

echo "#### direct: local preprocessing"
./timeit $CXX -E -c fast.cpp

echo "#### direct: local compilation"
./timeit $compile_cmd

echo "#### direct: remote compilation"
ssh $remote_host bash -c \
    "'. /cvmfs/lhcb.cern.ch/lib/LbEnv-stable; cd /tmp; ./timeit $compile_cmd'"

echo "#### distcc: using localhost"
DISTCC_HOSTS='localhost' ./timeit distcc $compile_cmd 

echo "#### distcc: standard mode"
./timeit distcc $compile_cmd

echo "#### distcc: pump mode"
(
    export DISTCC_HOSTS=$DISTCC_HOSTS_PUMP
    eval `pump --startup`
    ./timeit distcc $compile_cmd
    pump --shutdown >/dev/null
)


echo "### Parallel"
# echo "#### direct: local parallel overhead"
# ./timeit 3 ./parallel.sh 120 $nproc_local sleep 0

# echo "#### direct: local compilation"
# ./timeit 3 ./parallel.sh 120 $nproc_local $compile_cmd

# echo "#### direct: remote parallel overhead"
# ssh $remote_host bash -c \
#     "'. /cvmfs/lhcb.cern.ch/lib/LbEnv-stable; cd /tmp; 
#       ./timeit 3 ./parallel.sh 120 $nproc_remote sleep 0'"

echo "#### direct: remote compilation"
ssh $remote_host bash -c \
    "'. /cvmfs/lhcb.cern.ch/lib/LbEnv-stable; cd /tmp; 
      ./timeit 3 ./parallel.sh 120 $nproc_remote $compile_cmd'"

# echo "#### distcc: standard mode"
# ./timeit 3 ./parallel.sh 120 40 distcc $compile_cmd

echo "#### distcc: pump mode"
(
    export DISTCC_HOSTS=$DISTCC_HOSTS_PUMP
    eval `pump --startup`
    ./timeit 3 ./parallel.sh 120 40 distcc $compile_cmd
    pump --shutdown >/dev/null
)
